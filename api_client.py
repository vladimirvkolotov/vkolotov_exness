import json
import requests
from requests.adapters import HTTPAdapter
from requests.auth import HTTPBasicAuth
from urllib3 import Retry

from config import REQUEST_TIMEOUT


class ApiClient(object):
    def __init__(self, base_url, credentials=None, headers=None):
        self.base_url = base_url
        self.headers = {
            'Content-Type': 'text/html; charset=UTF-8',
            'cookie': 'PHPSESSID=ed8746e56e827b3e741493462731812a',
            'User-Agent': 'PostmanRuntime/7.15.0',
            'Cache-Control': 'no-cache',
            'Postman-Token': '88c5b300-29aa-47d9-ac3f-4fb7b2b81c10',
            'Host': 'dummy.restapiexample.com',
            'accept-encoding': 'gzip, deflate',
            'content-length': '64',
            'Connection': 'keep-alive',
            'Accept': '*/*'}
        if headers:
            self.headers.update(headers)
        self.auth_info = HTTPBasicAuth(*credentials) if credentials not in ((), None) else None

    def send_put(self, urn, data=None):
        return self.__send_request("PUT", urn, str(data))

    def send_get(self, urn, data=None):
        return self.__send_request("GET", urn, str(data))

    def send_post(self, urn, data, raw_response=False):
        return self.__send_request("POST", urn, data, raw_response)

    def send_delete(self, urn, data=None):
        return self.__send_request("DELETE", urn, data)

    def send_post_multipart_file(self, urn, file_data):
        response = requests.post(url=urn,
                                 auth=self.auth_info,
                                 files=file_data)
        if not response.ok:
            raise APIError(response.text)
        return response

    def __send_request(self, method, urn, data=None, raw_response=False):
        if data is not None and isinstance(data, bytes):
            data = data.decode('unicode_escape')
        elif data is not None:
            data = json.dumps(data)

        url = self.base_url + urn
        auth = self.auth_info if self.auth_info and self.auth_info.username else None

        with requests.Session() as s:
            retries = Retry(total=5,
                            backoff_factor=0.3,
                            status_forcelist=[500, 502, 503, 504])

            s.mount('https://', HTTPAdapter(max_retries=retries))
            response = s.request(method=method,
                                 url=url,
                                 auth=auth,
                                 timeout=REQUEST_TIMEOUT,
                                 data=data,
                                 headers=self.headers)
        if not response.ok:
            raise APIError(message=f'URL: {response.url} MESSAGE: {response.text}')
        if not raw_response:
            return self.handle_response(response, method=method)
        else:
            return response.text

    def handle_response(self, response, method=None):
        if not response.ok:
            error_content = json.loads(response.text)
            error = APIError(error_content)
            raise APIError("%s | %s" % (method, error.message), full_error=error,
                               status_code=response.status_code)
        if response.text != "":
            return response.json()
        else:
            return {}


class APIError(Exception):
    def __init__(self, message, full_error=None, status_code=None):
        super(APIError, self).__init__(message)
        self.message = message
        self.full_error = full_error
        self.status_code = status_code
