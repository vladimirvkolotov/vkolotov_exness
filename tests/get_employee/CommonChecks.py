import unittest
from random import randint
from django.utils.crypto import get_random_string
from tests.BaseTestCase import BaseTestCase
from config import CREATE_EMPLOYEE_URN, GET_EMPLOYEE_URN


class CommonChecksTestCase(BaseTestCase):
    def test_with_redundant_fields(self):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100)),
            "some_field_1": "Test"
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)


if __name__ == "__main__":
    unittest.main()
