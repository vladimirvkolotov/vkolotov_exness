import unittest
from parameterized import parameterized
from tests.BaseTestCase import BaseTestCase
from config import CREATE_EMPLOYEE_URN, GET_EMPLOYEE_URN
from django.utils.crypto import get_random_string
from random import randint, random


class NameFieldGetTestCase(BaseTestCase):
    def test_employee_with_valid_data(self):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)

    def test_employee_with_special_chars_in_name(self):
        # ARRANGE
        employee_name = self.special_chars_with_digits_string + get_random_string(15)
        request_data = {
            "name": employee_name,
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)

    def test_employee_with_name_more_than_255_symbols(self):
        # ARRANGE
        employee_name = get_random_string(256)
        request_data = {
            "name": employee_name,
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"][:100],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)

    def test_employee_with_sql_injection_at_name(self):
        # ARRANGE
        request_data = {
            "name": self.get_random_string(15) + " union show databases",
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)

    def test_employee_with_space_in_the_beginning_of_the_name(self):
        """The result of test is discussable.
        Because I think that it's better to trim the string"""
        # ARRANGE
        request_data = {
            "name": " " + self.get_random_string(15),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)

    def test_employee_with_space_in_the_end_of_the_name(self):
        """The result of test is discussable.
        Because I think that it's better to trim the string"""
        # ARRANGE
        request_data = {
            "name": self.get_random_string(15) + " ",
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)

    def test_employee_with_russian_symbols_at_name(self):
        # ARRANGE
        request_data = {
            "name": self.get_random_string(15) + "Тест",
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)


class NameFieldGetParametrizedTestCase(BaseTestCase):
    """I may move more cases from previous class there,
    but I don't do it, because it's better to run that tests separately"""

    @parameterized.expand(input=[(x,) for x in ("Nil",
                                                "None",
                                                "NULL",
                                                randint(10000000, 99999999),
                                                random(),
                                                )])
    def test_name_field(self, input_value):
        # ARRANGE
        request_data = {
            "name": str(input_value),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)


if __name__ == "__main__":
    unittest.main()
