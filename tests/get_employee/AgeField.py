import unittest
from parameterized import parameterized
from tests.BaseTestCase import BaseTestCase
from config import CREATE_EMPLOYEE_URN, GET_EMPLOYEE_URN
from django.utils.crypto import get_random_string
from random import randint


class AgeFieldGetTestCase(BaseTestCase):

    def test_string_with_special_chars_as_age(self):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
            "age": self.special_chars_with_digits_string
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)

    def test_256_digits_as_age(self):
        """Don't know how to estimate the expected result. Now this test is failed."""
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
            "age": get_random_string(256, "1234567890")
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": request_data["age"],
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)


class AgeFieldGetParametrizedTestCase(BaseTestCase):

    @parameterized.expand(input=[("10", "10"),
                                 ("Nil", 0),
                                 ("None", 0),
                                 ("NULL", 0),
                                 ("", 0),
                                 (123.344, 123),
                                 ("123,344", 123),
                                 (123.9, 123),
                                 (" ", 0),
                                 (0, 0),
                                 (-1, 0),
                                 (get_random_string(15), 0),
                                 ("99" + get_random_string(15, "abcd"), 0)
                                 ])
    def test_age_field(self, input_value, expected_result):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
            "age": str(input_value)
        }
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        expected_data = {
            "employee_name": request_data["name"],
            "employee_salary": request_data["salary"],
            "employee_age": str(expected_result),
            "profile_image": "",
            "id": response["id"]
        }
        # ACT
        response = self.api_client.send_get(GET_EMPLOYEE_URN + response["id"], request_data)
        # ASSERT
        self.assertDictEqual(response, expected_data)


if __name__ == "__main__":
    unittest.main()
