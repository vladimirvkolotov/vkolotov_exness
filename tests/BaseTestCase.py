import string
import unittest
from random import choice

from api_client import ApiClient
from config import WEB_SERVICE_BASE_URL, DELETE_EMPLOYEE_URN


class BaseTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.api_client = ApiClient(WEB_SERVICE_BASE_URL)
        cls.special_chars_with_digits_string = "!@#$%^&*()-_`+~=?/\\<>1234567890;':TESTtest"
        cls.maxDiff = None
        cls.employee_id_list = []

    @staticmethod
    def get_random_string(length):
        return str(''.join(choice(string.ascii_uppercase + string.digits) for _ in range(length)))

    def send_post(self, urn, data, raw_response=False):
        response = self.api_client.send_post(urn, data, raw_response)
        try:
            self.employee_id_list.append(response["id"])
        except Exception:
            pass
        return response


    @classmethod
    def tearDownClass(cls):
        """TearDown always returns 500 error and sometimes hangs"""
        for item in cls.employee_id_list:
            try:
                cls.api_client.send_delete(DELETE_EMPLOYEE_URN + f"/{item}")
            except Exception:
                pass
