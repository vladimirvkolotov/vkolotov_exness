import unittest
from random import randint
from django.utils.crypto import get_random_string
from api_client import APIError
from tests.BaseTestCase import BaseTestCase
from config import CREATE_EMPLOYEE_URN


class CommonChecksTestCase(BaseTestCase):
    def test_with_empty_json(self):
        self.assertRaises(APIError, self.send_post, CREATE_EMPLOYEE_URN, None)

    def test_with_redundant_fields(self):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100)),
            "some_field_1": "Test"
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)


if __name__ == "__main__":
    unittest.main()
