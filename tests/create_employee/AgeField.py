import unittest
from parameterized import parameterized
from tests.BaseTestCase import BaseTestCase
from config import CREATE_EMPLOYEE_URN
from django.utils.crypto import get_random_string
from random import randint, random


class AgeFieldPostTestCase(BaseTestCase):
    def test_age_is_not_unique(self):
        # ARRANGE
        age = str(randint(1, 100000))
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
            "age": age
        }
        self.api_client.send_post(CREATE_EMPLOYEE_URN, request_data)
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
            "age": age
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertDictEqual(response, request_data)

    def test_without_age_field(self):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
        }
        expected_message = "{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: " \
            "1048 Column 'employee_age' cannot be null}}"
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data, raw_response=True)
        # ASSERT
        self.assertEqual(response, expected_message)

    def test_string_with_special_chars_as_age(self):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
            "age": self.special_chars_with_digits_string
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)


class AgeFieldPostParametrizedTestCase(BaseTestCase):

    @parameterized.expand(input=[(x,) for x in ("Nil",
                                                "None",
                                                "NULL",
                                                "",
                                                str(randint(1, 100000) + random()),
                                                " ",
                                                0,
                                                -1,
                                                get_random_string(15),
                                                get_random_string(256, "1234567890"),
                                                str(randint(10, 100)) + "." + str(randint(10, 100)),
                                                str(randint(10, 100)) + "," + str(randint(10, 100)),
                                                get_random_string(100, "1234567890.,"),
                                                )])
    def test_age_field(self, input_value):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 1000)),
            "age": str(input_value)
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)


if __name__ == "__main__":
    unittest.main()
