import unittest
from parameterized import parameterized
from tests.BaseTestCase import BaseTestCase
from config import CREATE_EMPLOYEE_URN
from django.utils.crypto import get_random_string
from random import randint, random


class NameFieldPostTestCase(BaseTestCase):
    def test_create_employee_with_valid_data(self):
        # ARRANGE
        request_data = {
            "name": get_random_string(15),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)

    def test_without_name_field(self):
        # ARRANGE
        request_data = {
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        expected_message = "{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: " \
            "1048 Column 'employee_name' cannot be null}}"
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data, raw_response=True)
        # ASSERT
        self.assertEqual(response, expected_message)

    def test_create_employee_with_existing_name(self):
        """ Wrong response code (200) and wrong json structure at the response.
        Now TestCase is passed to fix the current behaviour and to be able to catch regression bugs.
        But it must be refactored after fix of the response JSON structure and response code.
        """
        # ARRANGE
        employee_name = get_random_string(15)
        request_data = {
            "name": employee_name,
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        expected_data = "{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: " \
                        "1062 Duplicate entry '%s' for key 'employee_name_unique'}}" % employee_name
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data, raw_response=True)
        # ASSERT
        self.assertEqual(response, expected_data)

    def test_create_employee_with_empty_name(self):
        """ Wrong response code (200) and wrong json structure at the response.
        Also message should contain text about empty name
        Now TestCase is passed to fix the current behaviour and to be able to catch regression bugs.
        But it must be refactored after fix of the web service.
        """
        # ARRANGE
        employee_name = ""
        request_data = {
            "name": employee_name,
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        expected_data = "{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: " \
                        "1062 Duplicate entry '%s' for key 'employee_name_unique'}}" % employee_name
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data, raw_response=True)
        # ASSERT
        self.assertEqual(response, expected_data)

    def test_create_employee_with_special_chars_in_name(self):
        # ARRANGE
        employee_name = self.special_chars_with_digits_string + get_random_string(15)
        request_data = {
            "name": employee_name,
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        self.assertTrue(int(response["id"]))
        request_data["id"] = response["id"]
        self.assertEqual(response, request_data)

    def test_create_employee_with_name_more_than_255_symbols(self):
        # ARRANGE
        employee_name = get_random_string(256)
        request_data = {
            "name": employee_name,
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        self.assertTrue(int(response["id"]))
        request_data["id"] = response["id"]
        self.assertEqual(response, request_data)

    def test_create_employee_with_sql_injection_at_name(self):
        # ARRANGE
        request_data = {
            "name": self.get_random_string(15) + " union show databases",
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)

    def test_create_employee_with_space_in_the_beginning_of_the_name(self):
        """The result of test is discussable.
        Because I think that it's better to trim the string"""
        # ARRANGE
        request_data = {
            "name": " " + self.get_random_string(15),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)

    def test_create_employee_with_space_in_the_end_of_the_name(self):
        """The result of test is discussable.
        Because I think that it's better to trim the string"""
        # ARRANGE
        request_data = {
            "name": self.get_random_string(15) + " ",
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)

    def test_create_employee_with_russian_symbols_at_name(self):
        # ARRANGE
        request_data = {
            "name": self.get_random_string(15) + "Тест",
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)


class NameFieldPostParametrizedTestCase(BaseTestCase):
    """I may move more cases from previous class there,
    but I don't do it, because it's better to run that tests separately"""

    @parameterized.expand(input=[(x,) for x in ("Nil",
                                                "None",
                                                "NULL",
                                                randint(10000000, 99999999),
                                                random(),
                                                )])
    def test_name_field(self, input_value):
        # ARRANGE
        request_data = {
            "name": str(input_value),
            "salary": str(randint(1, 100000)),
            "age": str(randint(1, 100))
        }
        # ACT
        response = self.send_post(CREATE_EMPLOYEE_URN, request_data)
        # ASSERT
        request_data["id"] = response["id"]
        self.assertTrue(int(response["id"]))
        self.assertDictEqual(response, request_data)


if __name__ == "__main__":
    unittest.main()
